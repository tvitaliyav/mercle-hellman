package controller;

import util.MerkleHellman;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

public class Controller {

    public static void run(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите n");
        int n = scanner.nextInt();
        System.out.println("Введите t");
        int t = scanner.nextInt();
        System.out.println("Введите сообщение длины n");
        Scanner s = new Scanner(System.in);
        String line = s.nextLine();
        int[] msg = new int[n];
        char[] tmp = line.toCharArray();
        for (int i = 0; i < n ; i++) {
            if (tmp[i] == '1') {
                msg[i] = 1;
            }
            if (tmp[i] == '0') {
                msg[i] = 0;
            }

        }
        MerkleHellman merkleHellman = new MerkleHellman(n, t);
        merkleHellman.generate();
        long c = merkleHellman.encrypt(msg);
        System.out.println(c);
        System.out.println(Arrays.toString(merkleHellman.decrypt(c)));
    }
}

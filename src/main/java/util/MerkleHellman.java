package util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.apache.commons.math3.util.ArithmeticUtils.gcd;
import static org.apache.commons.math3.util.ArithmeticUtils.pow;

public class MerkleHellman {

    int n;
    int t;
    int[] p;
    long[] M;
    long[] W;
    long[][] aj;

    public MerkleHellman(int n, int t) {
        this.n = n;
        this.t = t;
        p = new int[n];
        M = new long[t];
        W = new long[t];
        aj = new long[t + 1][n];
    }

    private void sequence() {
        Random random = new Random();
        aj[0][0] = random.nextInt(15);
        long sum = aj[0][0];
        for (int i = 1; i < n; i++) {
            aj[0][i] = random.nextInt(15) + sum + 1;
            sum += aj[0][i];
        }
    }

    private void recount() {
        Random random = new Random();
        for (int j = 1; j < t + 1; j++) {
            long sum = 0;
            for (int i = 0; i < n; i++) {
                sum += aj[j - 1][i];
            }
            M[j - 1] = random.nextInt(20) + 1 + sum;
            W[j - 1] = (long) (Math.random() * (M[j - 1] - 1));
            while (W[j - 1] == 0 || gcd(M[j - 1], W[j - 1]) != 1) {
                W[j - 1] = (long) (Math.random() * (M[j - 1] - 1));
            }
            for (int i = 0; i < n; i++) {
                aj[j][i] = (aj[j - 1][i] * W[j - 1]) % M[j - 1];
            }
        }
    }

    private void transposition() {
        Random random = new Random();
        List<Integer> tmp = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int ind = random.nextInt(n);
            while (tmp.contains(ind)) {
                ind = random.nextInt(n);
            }
            tmp.add(ind);
            p[i] = ind;
        }
    }

    public void generate() {
        sequence();
        recount();
        transposition();
    }

    public long encrypt(int[] m) {
        long[] key = getPublicKey();
        long res = 0;
        for (int i = 0; i < n; i++) {
            res += key[i] * m[i];
        }
        return res;
    }

    private long[] getPublicKey() {
        long[] k = new long[n];
        for (int i = 0; i < n; i++) {
            k[i] = aj[t][p[i]];
        }
        return k;
    }

    public int[] decrypt(long c) {
        BigInteger[] d = new BigInteger[t+1];
        d[t] = BigInteger.valueOf(c);
        for (int i = t - 1; i > -1; i--) {
            d[i] = (d[i+1].multiply(BigInteger.valueOf(Inverse.inverse(W[i], M[i])))).
                    mod( BigInteger.valueOf(M[i]));
        }
        int[] tmp = Knapsack(d[0].longValue());
        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            res[i] = tmp[p[i]];
        }
        return res;
    }

    private int[] Knapsack(long d) {
        int[] res = new int[n];
        long sum = 0;
        for (int i = n-1; i > -1; i--) {
            if (aj[0][i] + sum > d) {
                res[i] = 0;
            } else {
                res[i] = 1;
                sum+= aj[0][i];
            }
        }
        return res;
    }
}

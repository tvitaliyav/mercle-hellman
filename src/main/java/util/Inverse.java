package util;

public class Inverse {

    public static long[] extendedEuclid(long a, long b) {
        long q, r, x1, x2, y1, y2;
        long[] res = new long[3];
        if (b == 0) {

            res[0] = a;
            res[1] = 1;
            res[2] = 0;

            return res;
        }

        x2 = 1;
        x1 = 0;
        y2 = 0;
        y1 = 1;

        while (b > 0) {

            q = a / b;
            r = a - q * b;

            res[1] = x2 - q * x1;
            res[2] = y2 - q * y1;

            a = b;
            b = r;

            x2 = x1;
            x1 = res[1];
            y2 = y1;
            y1 = res[2];

        }

        res[0] = a;
        res[1] = x2;
        res[2] = y2;

        return res;
    }


    public static long inverse(long a, long n) {

        long[] res = extendedEuclid(a, n);

        if (res[0] == 1) return res[1] <0 ? n + res[1] : res[1];

        return 0;

    }


//    int main(void)
//
//    {
//
////        long a = 5, n = 7;
//
//
////        printf("the inverse of %ld modulo %2ld is %ld\n", a, n, inverse(a, n));
//
////        a = 2, n = 12;
//
////        printf("the inverse of %ld modulo %2ld is %ld\n", a, n, inverse(a, n));
//
//        return 0;
//    }
}
